import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import Home from "../pages/Home/Home";
import {useDispatch, useSelector} from 'react-redux';
import * as productsSelectors from '../store/products/selectors';
import * as productsActions from '../store/products/actions';
import * as errorSelectors from '../store/error/selectors';
import * as modalSelectors from '../store/modalWindow/selectors';
import * as modalWindowActions from '../store/modalWindow/actions'
import Favorites from "../pages/Favorites/Favorites";
import Cart from "../pages/Cart/Cart";
import Page404 from "../pages/Page404/Page404";


const AppRoutes = (props) => {


    const {products} = props;
    const dispatch = useDispatch();
    const productsInBin = useSelector(productsSelectors.getProductsInCartSelector);
    const lovelyProducts = useSelector(productsSelectors.getFavoritesProductsSelector);
    const isModal = useSelector(modalSelectors.getIsModalSelector);
    const modalInfo = useSelector(modalSelectors.getModalInfoSelector);
    const isLoading = useSelector(productsSelectors.getProductsLoadingSelector);
    const error = useSelector(errorSelectors.getErrorSelector);
    const toggleLovelyProduct = (article) => dispatch(productsActions.toggleFavoriteAction(article));
    const toggleBin = (article) => dispatch(productsActions.toggleProductsInCartAction(article));
    const toggleModalWindow = (modalInfo) => dispatch(modalWindowActions.toggleModalWindow(modalInfo));



    return (
        <Switch>
            <Redirect exact from='/' to='/home'/>
            <Route exact path='/home'><Home products={products} productsInBin={productsInBin}
                                            lovelyProducts={lovelyProducts}
                                            isModal={isModal} modalInfo={modalInfo}
                                            toggleLovelyProduct={toggleLovelyProduct}
                                            toggleModalWindow={toggleModalWindow} toggleBin={toggleBin} error={error}/></Route>
            <Route exact path='/favorites'><Favorites isLoading={isLoading} isModal={isModal} toggleBin={toggleBin}
                                                      modalInfo={modalInfo}
                                                      toggleLovelyProduct={toggleLovelyProduct}
                                                      productsInBin={productsInBin}
                                                      lovelyProducts={lovelyProducts} products={products}
                                                      toggleModalWindow={toggleModalWindow} error={error}/></Route>
            <Route exact path='/cart'><Cart isLoading={isLoading} isModal={isModal} toggleBin={toggleBin}
                                            modalInfo={modalInfo}
                                            toggleLovelyProduct={toggleLovelyProduct} lovelyProducts={lovelyProducts}
                                            products={products} toggleModalWindow={toggleModalWindow}
                                            productsInBin={productsInBin} error={error}/></Route>
            <Route path='*' component={Page404}/>
        </Switch>
    );
};

export default AppRoutes;