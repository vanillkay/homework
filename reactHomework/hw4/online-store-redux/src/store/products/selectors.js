export const getProductsSelector = ({products}) => products.products;
export const getProductsLoadingSelector = ({products}) => products.isLoading;
export const getProductsInCartSelector = ({products}) => products.productsInCart;
export const getFavoritesProductsSelector = ({products}) => products.favoritesProducts;