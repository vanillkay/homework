import React, {useEffect} from 'react';
import './ModalWindow.scss';
import PropTypes from 'prop-types';
import Button from "../Button/Button";

const ModalWindow = (props) => {
    const keys = {37: 1, 38: 1, 39: 1, 40: 1};
    let wheelOpt, wheelEvent
    const {modalInfo, productsInBin, toggleModalWindow, toggleBin} = props;
    const {name, price, color, article} = modalInfo || {name: 'product', price: 'no price', color: ''};
    const isInBin = productsInBin.includes(article);


    const preventDefault = (e) => {
        e.preventDefault();
    }

    const preventDefaultForScrollKeys = (e) => {
        if (keys[e.keyCode]) {
            e.preventDefault();
            return false;
        }
    }


    const disableScroll = () => {
        window.addEventListener('DOMMouseScroll', preventDefault, false);
        window.addEventListener(wheelEvent, preventDefault, wheelOpt);
        window.addEventListener('touchmove', preventDefault, wheelOpt);
        window.addEventListener('keydown', preventDefaultForScrollKeys, false);
    }


    const enableScroll = () => {
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.removeEventListener(wheelEvent, preventDefault, wheelOpt);
        window.removeEventListener('touchmove', preventDefault, wheelOpt);
        window.removeEventListener('keydown', preventDefaultForScrollKeys, false);
    }


    useEffect(() => {
        let supportsPassive = false;
        try {
            window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
                get: function () { supportsPassive = true;}
            }));
        } catch(e) {}

        wheelOpt = supportsPassive ? { passive: false } : false;
        wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';
        disableScroll();
        return enableScroll;
    })

    return (
        <>
            <div className="modal__window">
                <div className='modal'>
                    <div className="modal__header">
                        <div
                            className="modal__title">{isInBin ? `${color} ${name} now in cart` : `Are you sure to buy this ${name} ?`}</div>
                        <Button action={toggleModalWindow} text='X' backgroundCol='black'/>
                    </div>
                    <div className="modal__content">{color} {name}, price - {price}</div>
                    <div className="modal__actions"><Button action={() => {
                        toggleBin(article);
                        toggleModalWindow();
                    }} backgroundCol={isInBin ? 'green' : ''} text={isInBin ? 'DELETE FROM CART ?' : 'BUY'}/></div>
                </div>
            </div>
        </>
    );
}




ModalWindow.propTypes = {
    modalInfo: PropTypes.object,
    toggleModalWindow: PropTypes.func.isRequired,
    toggleBin: PropTypes.func.isRequired
}

ModalWindow.defaultProps = {
    modalInfo: {}
}
export default ModalWindow;