import React from 'react';
import Product from "../Product/Product";
import PropTypes from 'prop-types';
import './ProductsList.scss';

const ProductsList = (props) => {
    const {productsInBin, products, lovelyProducts, toggleLovelyProduct, toggleModalWindow, page, error} = props;
    const productsCards = products.map(item => {
        const isLovely = lovelyProducts.includes(item.article);
        return <Product isLovely={isLovely} key={item.article} toggleModalWindow={toggleModalWindow}
                        toggleLovelyProduct={toggleLovelyProduct} {...item} productsInBin={productsInBin}/>
    });
    return (
        <div className="container">
            <div className="products__title">{page} products</div>
            <div className='products'>
                {!productsCards.length && ((error && <h2 className='products__no-products'>{error}</h2>) || <h2 className='products__no-products'>NO PRODUCTS</h2>)}
                {productsCards}
            </div>
        </div>
    );
}

ProductsList.propTypes = {
    products: PropTypes.array,
    lovelyProducts: PropTypes.array,
    toggleModalWindow: PropTypes.func.isRequired,
    toggleLovelyProduct: PropTypes.func.isRequired
}

ProductsList.defaultProps = {
    products: [],
    lovelyProducts: []
}

export default ProductsList;