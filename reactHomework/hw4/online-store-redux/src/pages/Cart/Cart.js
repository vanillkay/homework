import React from 'react';
import ProductsList from "../../components/ProductsList/ProductsList";
import ModalWindow from "../../components/ModalWindow/ModalWindow";
import PropTypes from 'prop-types';
import Loader from "../../components/Loader/Loader";

const Cart = (props) => {
    const {isModal, modalInfo, toggleModalWindow, toggleLovelyProduct, lovelyProducts, toggleBin, productsInBin, products, error} = props;
    const prodInBin = products.filter(({article}) => productsInBin && productsInBin.includes(article));

    return (
        <div>
            <ProductsList toggleLovelyProduct={toggleLovelyProduct} toddleBin={toggleBin}
                          toggleModalWindow={toggleModalWindow} lovelyProducts={lovelyProducts}
                          products={prodInBin} productsInBin={productsInBin}
                          page='In cart' error={error}/>

            {isModal &&
            <ModalWindow productsInBin={productsInBin} toggleBin={toggleBin} toggleModalWindow={toggleModalWindow}
                         modalInfo={modalInfo}/>}
        </div>
    );
};

Cart.propTypes = {
    isModal: PropTypes.bool.isRequired,
    modalInfo: PropTypes.object.isRequired,
    toggleModalWindow: PropTypes.func.isRequired,
    toggleLovelyProduct: PropTypes.func.isRequired,
    lovelyProducts: PropTypes.array,
    toggleBin: PropTypes.func.isRequired,
    productsInBin: PropTypes.array.isRequired,
    products: PropTypes.array.isRequired,
    error: PropTypes.any
}

Cart.defaultProps = {
    lovelyProducts: []
}
export default Cart;