import React from 'react';
import Loader from "../../components/Loader/Loader";
import ProductsList from "../../components/ProductsList/ProductsList";
import ModalWindow from "../../components/ModalWindow/ModalWindow";
import PropTypes from 'prop-types';


const Home = (props) => {
    const {isLoading, productsInBin, isModal, toggleModalWindow, toggleLovelyProduct, lovelyProducts, products, toggleBin, modalInfo, error} = props;
    return (
        <div>
            <ProductsList
                toggleModalWindow={toggleModalWindow}
                toggleLovelyProduct={toggleLovelyProduct}
                lovelyProducts={lovelyProducts}
                products={products}
                page='Our'
                error={error}
                productsInBin={productsInBin}
            />
            {isModal &&
            <ModalWindow productsInBin={productsInBin} toggleBin={toggleBin} toggleModalWindow={toggleModalWindow}
                         modalInfo={modalInfo}/>}

        </div>
    );
};

Home.propTypes = {
    isLoading: PropTypes.bool,
    isModal: PropTypes.bool.isRequired,
    modalInfo: PropTypes.object.isRequired,
    toggleModalWindow: PropTypes.func.isRequired,
    toggleLovelyProduct: PropTypes.func.isRequired,
    lovelyProducts: PropTypes.array.isRequired,
    products: PropTypes.array.isRequired,
    toggleBin: PropTypes.func.isRequired,
    error: PropTypes.any
}

export default Home;