import './App.css';
import React, {useEffect, useState} from 'react';
import NavBar from "./components/NavBar/NavBar";
import AppRoutes from "./routes/AppRoutes";
import axios from "axios";
import Loader from "./components/Loader/Loader";


const App = () => {

    const [products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [lovelyProducts, setLovelyProducts] = useState([]);
    const [productsInBin, setProductsInBin] = useState([]);
    const [isModal, setIsModal] = useState(false);
    const [modalInfo, setModalInfo] = useState({});
    const [error, setError] = useState(undefined);


    const localStorageCheck = (checkParam) => {
        const isInLocalStorage = localStorage.getItem(checkParam) !== null;
        if (isInLocalStorage) {
            return JSON.parse(localStorage.getItem(`${checkParam}`));
        } else {
            localStorage.setItem(`${checkParam}`, JSON.stringify([]));
            return [];
        }
    }

    const toggleProduct = (products, prodArticle) => {
        const isInProductList = products.includes(prodArticle);
        if (isInProductList) {
            const position = products.indexOf(prodArticle);
            products.splice(position, 1);
            return [...products];
        }else{
            return [...products, prodArticle];
        }
    }

    const toggleLovelyProduct = (prodArticle) => {
        const newLovelyProducts = toggleProduct(lovelyProducts, prodArticle);
        localStorage.setItem('lovelyProducts', JSON.stringify(newLovelyProducts));
        setLovelyProducts(newLovelyProducts);
    }

    const toggleBin = (prodArticle) => {
        const newProductsInBin = toggleProduct(productsInBin, prodArticle);
        localStorage.setItem('productsInBin', JSON.stringify(newProductsInBin));
        setIsModal(!isModal);
        setModalInfo({});
        setProductsInBin(newProductsInBin);
    }

    useEffect(() => {

        const newLovelyProducts = localStorageCheck('lovelyProducts');
        const newProductsInBin = localStorageCheck('productsInBin');

        setLovelyProducts(newLovelyProducts);
        setProductsInBin(newProductsInBin);

        axios('/products/product-list/product-list.json')
            .then(response => {
                setIsLoading(false);
                setProducts(response.data)
            }).catch(error => {
            setIsLoading(false);
            setError('SERVER DOESN\'T WORK!!!  TRY AGAIN LATER');
        });
    }, []);


    const toggleModalWindow = (modalParams) => {
        const modalInfo = isModal ? {} : modalParams;
        setIsModal(!isModal);
        setModalInfo(modalInfo);
    }


    return (
        <>
            <NavBar/>
            {isLoading ? <Loader/> : <AppRoutes
                error={error}
                products={products}
                isLoading={isLoading}
                lovelyProducts={lovelyProducts}
                productsInBin={productsInBin}
                isModal={isModal}
                modalInfo={modalInfo}
                toggleLovelyProduct={toggleLovelyProduct}
                toggleModalWindow={toggleModalWindow}
                toggleBin={toggleBin}/>
            }
        </>
    )
}

export default App;
