import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types';


const Button = (props) =>{
    const {backgroundCol, text, action} = props;
    return (
        <button  onClick={() => action()} style={{backgroundColor: backgroundCol}} className='button'>{text}</button>
    );

}


Button.propTypes = {
    backgroundCol: PropTypes.string,
    text: PropTypes.string,
    action: PropTypes.func.isRequired
}

Button.defaultProps = {
    backgroundCol: 'black',
    text: 'Push me'
}

export default Button;