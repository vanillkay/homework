import React from 'react';
import { NavLink } from 'react-router-dom';
import './NavBar.scss';

const NavBar = (props) => {
    return (
        <ul className='navBar'>
            <li><NavLink className='navBar__link' exact to='/home'>HOME</NavLink></li>
            <li><NavLink className='navBar__link' exact to='/favorites'>FAVORITES</NavLink></li>
            <li><NavLink className='navBar__link' exact to='/cart' >CART</NavLink></li>
        </ul>
    );
};

export default NavBar;