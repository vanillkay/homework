import React from 'react';
import './Page404.scss'

const Page404 = () => {
    return (
        <div className='page-404'>Page not found 404</div>
    );
};

export default Page404;