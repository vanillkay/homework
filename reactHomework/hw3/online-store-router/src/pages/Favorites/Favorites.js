import React from 'react';
import ProductsList from "../../components/ProductsList/ProductsList";
import ModalWindow from "../../components/ModalWindow/ModalWindow";
import PropTypes from 'prop-types';
import Loader from "../../components/Loader/Loader";

const Favorites = (props) => {
    const {isLoading, productsInBin, isModal, toggleBin, modalInfo, toggleModalWindow, toggleLovelyProduct, lovelyProducts, products, error} = props;
    const lovProducts = products.filter(({article}) => lovelyProducts && lovelyProducts.includes(article));
    return (
        <div>
            {
                isLoading ? <Loader/> : <ProductsList
                    toggleLovelyProduct={toggleLovelyProduct}
                    toggleModalWindow={toggleModalWindow}
                    lovelyProducts={lovelyProducts}
                    products={lovProducts}
                    productsInBin={productsInBin}
                    page='Your favorites' error={error}/>
            }
            {isModal &&
            <ModalWindow productsInBin={productsInBin} toggleBin={toggleBin}  toggleModalWindow={toggleModalWindow} modalInfo={modalInfo}/>}
        </div>
    );
};

Favorites.propTypes = {
    isModal: PropTypes.bool.isRequired,
    modalInfo: PropTypes.object.isRequired,
    toggleModalWindow: PropTypes.func.isRequired,
    toggleLovelyProduct: PropTypes.func.isRequired,
    lovelyProducts: PropTypes.array.isRequired,
    toggleBin: PropTypes.func.isRequired,
    productsInBin: PropTypes.array,
    products: PropTypes.array.isRequired,
    error: PropTypes.any
}

Favorites.defaultProps = {
    productsInBin: []
}

export default Favorites;