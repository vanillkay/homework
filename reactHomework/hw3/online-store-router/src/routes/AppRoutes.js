import React from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import Home from "../pages/Home/Home";
import Favorites from "../pages/Favorites/Favorites";
import Cart from "../pages/Cart/Cart";
import Page404 from "../pages/Page404/Page404";
import PropTypes from 'prop-types';


const AppRoutes = (props) => {
    const {isLoading, productsInBin, isModal, toggleModalWindow, toggleLovelyProduct, lovelyProducts, products, toggleBin, modalInfo, error} = props;
    return (
        <Switch>
            <Redirect exact from='/' to='/home'/>
            <Route exact path='/home'><Home products={products} productsInBin={productsInBin} isLoading={isLoading} lovelyProducts={lovelyProducts}
                                            isModal={isModal} modalInfo={modalInfo}
                                            toggleLovelyProduct={toggleLovelyProduct}
                                            toggleModalWindow={toggleModalWindow} toggleBin={toggleBin} error={error}/></Route>
            <Route exact path='/favorites'><Favorites isLoading={isLoading} isModal={isModal} toggleBin={toggleBin} modalInfo={modalInfo}
                                                      toggleLovelyProduct={toggleLovelyProduct} productsInBin={productsInBin}
                                                      lovelyProducts={lovelyProducts} products={products}
                                                      toggleModalWindow={toggleModalWindow} error={error}/></Route>
            <Route exact path='/cart'><Cart isLoading={isLoading} isModal={isModal} toggleBin={toggleBin} modalInfo={modalInfo}
                                            toggleLovelyProduct={toggleLovelyProduct} lovelyProducts={lovelyProducts}
                                            products={products} toggleModalWindow={toggleModalWindow}
                                            productsInBin={productsInBin} error={error}/></Route>
            <Route path='*' component={Page404}/>
        </Switch>
    );
};


AppRoutes.propTypes ={
    isLoading: PropTypes.bool,
    isModal: PropTypes.bool.isRequired,
    modalInfo: PropTypes.object.isRequired,
    toggleModalWindow: PropTypes.func.isRequired,
    toggleLovelyProduct: PropTypes.func.isRequired,
    lovelyProducts: PropTypes.array.isRequired,
    products: PropTypes.array.isRequired,
    productsInBin: PropTypes.array.isRequired,
    toggleBin: PropTypes.func.isRequired,
    error: PropTypes.any
}


export default AppRoutes;