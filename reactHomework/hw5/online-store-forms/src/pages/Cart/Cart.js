import React, {useState} from 'react';
import ProductsList from "../../components/ProductsList/ProductsList";
import ModalWindow from "../../components/ModalWindow/ModalWindow";
import Button from '../../components/Button/Button'
import PropTypes from 'prop-types';
import MyForm from "../../components/Form/Form";
import {useSelector} from "react-redux";
import {getProductsInCartSelector} from '../../store/products/selectors';


const Cart = (props) => {
    const {isModal, modalInfo, toggleModalWindow, toggleLovelyProduct, lovelyProducts, toggleBin, productsInBin, products, error} = props;
    const prodInBin = products.filter(({article}) => productsInBin && productsInBin.includes(article));

    const [isForm, setIsForm] = useState(false);

    const productsInCart = useSelector(getProductsInCartSelector);


    return (
        <div>
            {!!productsInCart.length && <Button action={() => setIsForm(true)} text='make the order'/>}
            <ProductsList toggleLovelyProduct={toggleLovelyProduct} toddleBin={toggleBin}
                          toggleModalWindow={toggleModalWindow} lovelyProducts={lovelyProducts}
                          products={prodInBin} productsInBin={productsInBin}
                          page='In cart' error={error}/>

            {isModal &&
            <ModalWindow productsInBin={productsInBin} toggleBin={toggleBin} toggleModalWindow={toggleModalWindow}
                         modalInfo={modalInfo}/>}
            {isForm &&
            <ModalWindow toggleBin={toggleBin} toggleModalWindow={toggleModalWindow} toggleForm={setIsForm} isForm={true}/>}
        </div>
    );
};

Cart.propTypes = {
    isModal: PropTypes.bool.isRequired,
    modalInfo: PropTypes.object.isRequired,
    toggleModalWindow: PropTypes.func.isRequired,
    toggleLovelyProduct: PropTypes.func.isRequired,
    lovelyProducts: PropTypes.array,
    toggleBin: PropTypes.func.isRequired,
    productsInBin: PropTypes.array.isRequired,
    products: PropTypes.array.isRequired,
    error: PropTypes.any
}

Cart.defaultProps = {
    lovelyProducts: []
}
export default Cart;