import './App.css';
import React, {useEffect} from 'react';
import NavBar from "./components/NavBar/NavBar";
import AppRoutes from "./routes/AppRoutes";
import {useDispatch, useSelector} from "react-redux";
import * as productsSelectors from "./store/products/selectors";
import Loader from "./components/Loader/Loader";
import {loadProducts} from "./store/products/operations";


const App = () => {

    const isLoading = useSelector(productsSelectors.getProductsLoadingSelector);
    const products = useSelector(productsSelectors.getProductsSelector);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!products.length) {
            dispatch(loadProducts());
        }
    }, [])

    return (
        <>
            <NavBar/>
            {isLoading ? <Loader/> : <AppRoutes products={products}/>}
        </>
    )
}

export default App;
