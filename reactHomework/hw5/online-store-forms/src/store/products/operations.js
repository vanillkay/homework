import axios from 'axios';
import {productsLoadingAction, saveProductsAction, showBuyingProducts} from './actions';
import {setError} from "../error/actions";

export const loadProducts = () => (dispatch, getState) => {
    dispatch(productsLoadingAction(true))
    axios('/products/product-list/product-list.json')
        .then(res => {
            dispatch(saveProductsAction(res.data));
        }).catch(error => {
        dispatch(setError('SERVER DOESN\'T WORK!!!  TRY AGAIN LATER'));
    }).finally(dispatch(productsLoadingAction(false)));

}

const toggleProduct = (products, prodArticle) => {
    const isInProductList = products.includes(prodArticle);
    if (isInProductList) {
        const position = products.indexOf(prodArticle);
        products.splice(position, 1);
        return [...products];
    } else {
        return [...products, prodArticle];
    }
}

export const toggleLovelyProduct =  (lovelyProducts, prodArticle) => {
    const newLovelyProducts = toggleProduct(lovelyProducts, prodArticle);
    localStorage.setItem('lovelyProducts', JSON.stringify(newLovelyProducts));
    return newLovelyProducts;
}

export const toggleProductsInCart = (productsInBin, prodArticle)  => {
    const newProductsInBin = toggleProduct(productsInBin, prodArticle);
    localStorage.setItem('productsInBin', JSON.stringify(newProductsInBin));
    return newProductsInBin;
}

export const localStorageCheck = (checkParam) => {
    const isInLocalStorage = localStorage.getItem(checkParam) !== null;
    if (isInLocalStorage) {
        return JSON.parse(localStorage.getItem(`${checkParam}`));
    } else {
        localStorage.setItem(`${checkParam}`, JSON.stringify([]));
        return [];
    }
}





