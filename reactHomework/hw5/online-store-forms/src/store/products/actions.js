import * as productsActionsTypes from './types';

export const productsLoadingAction = (isLoading) => ({
    type: productsActionsTypes.PRODUCTS_LOADING,
    payload: isLoading
})

export const saveProductsAction = (products) => ({
    type: productsActionsTypes.SAVE_PRODUCTS,
    payload: products
})

export const toggleFavoriteAction = (article) => ({
    type: productsActionsTypes.TOGGLE_FAVORITES,
    payload: article
})

export const toggleProductsInCartAction = (article) => ({
    type: productsActionsTypes.TOGGLE_PRODUCTS_IN_CART,
    payload: article
})

export const clearBinAction = () => ({
    type: productsActionsTypes.CLEAR_BIN,
})

export const showBuyingProducts = (allProducts) => ({
    type: productsActionsTypes.SHOW_BUYING_PRODUCTS,
    payload: allProducts
})

