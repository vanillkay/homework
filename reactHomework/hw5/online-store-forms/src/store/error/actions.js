import * as errorActionsTypes from './types';

export const setError = error => ({
    type: errorActionsTypes.SET_ERROR,
    payload: error
})