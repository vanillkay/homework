import * as errorActionTypes from './types';

const initialState = {
    error: undefined
}

const reducer = (state = initialState,  action) => {
    switch (action.type){
        case errorActionTypes.SET_ERROR:
            return {...state, error: action.payload};
        default: return state;
    }
};

export default reducer;