import * as modalWindowTypes from './types'

export const setIsModal = (isModal) => ({
    type: modalWindowTypes.SET_IS_MODAL,
    payload: isModal
});

export const setModalInfo = (modalInfo) => ({
    type: modalWindowTypes.SET_MODAL_INFO,
    payload: modalInfo
});


export const toggleModalWindow = (modalParams) => ({
    type: modalWindowTypes.TOGGLE_MODAL_WINDOW,
    payload: modalParams
})