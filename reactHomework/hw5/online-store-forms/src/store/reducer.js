import { combineReducers } from 'redux';
import products from './products/reducer';
import modalWindow from './modalWindow/reducer';
import error from './error/reducer'

const rootReducer = combineReducers({
    products,
    modalWindow,
    error
})

export default rootReducer;
