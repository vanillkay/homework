import React from 'react';
import './Button.scss';
import PropTypes from 'prop-types';


const Button = (props) =>{
    const {backgroundCol, text, action} = props;
    return (
        <button  onClick={() => action()} style={{backgroundColor: backgroundCol}} className='button'>{text.toUpperCase()}</button>
    );

}


Button.propTypes = {
    backgroundCol: PropTypes.string,
    text: PropTypes.string,
    action: PropTypes.func
}

Button.defaultProps = {
    backgroundCol: 'black',
    text: 'Push me',
    action: () => {}
}

export default Button;