import React from 'react';
import {Form, Formik} from 'formik';
import Input from './Input/Input';
import * as yup from 'yup';
import {clearBinAction} from "../../store/products/actions";
import {useDispatch, useSelector} from 'react-redux';
import {getProductsSelector, getProductsInCartSelector} from "../../store/products/selectors";

const FIELD_REQUIRED = 'This field is required!!!'
const schema = yup.object().shape({
    name: yup
        .string()
        .required(FIELD_REQUIRED)
        .matches(/^[A-zА-яЁё]+$/, 'Вводите только буквы'),
    surname: yup
        .string()
        .required(FIELD_REQUIRED)
        .matches(/^[A-zА-яЁё]+$/, 'Вводите только буквы'),
    age: yup
        .string()
        .matches(/[0-9]/, 'Вводите только цифры')
        .required(FIELD_REQUIRED),
    delivery: yup
        .string()
        .required(FIELD_REQUIRED),
    phone: yup
        .string()
        .matches(/[0-9]/, 'Вводите только цифры')
        .matches(/^((\+?3)?8)?((0\(\d{2}\)?)|(\(0\d{2}\))|(0\d{2}))\d{7}$/, 'Введите номер в формате 0*********')
        .required(FIELD_REQUIRED),

});


const MyForm = (props) => {
   const {toggleForm} = props;
    const dispatch = useDispatch();
    const allProducts = useSelector(getProductsSelector);
    const productsInCart = useSelector(getProductsInCartSelector);

    const handleSubmit = (values, {setSubmitting}) => {
        setSubmitting(false);
        dispatch(clearBinAction());
        const buyingProducts = allProducts.filter(item => productsInCart.includes(item.article));
        console.log('Users inputs -> ', values);
        console.log('Buying products -> ', buyingProducts);
        setSubmitting(true);
        toggleForm(false);

    }
    return (
        <Formik validationSchema={schema}
                onSubmit={handleSubmit}
                initialValues={{
                    name: '',
                    surname: '',
                    age: '',
                    delivery: '',
                    phone: ''
                }}>
            <Form className='login' noValidate>
                <Input label='name' type='text' placeholder='name' name='name'/>
                <Input label='surname' type='text' placeholder='surname' name='surname'/>
                <Input label='age' type='text' placeholder='your age' name='age'/>
                <Input label='Delivery place' type='text' placeholder='your delivery place' name='delivery'/>
                <Input label='phone' type='text' placeholder='your phone' name='phone'/>
                <div>
                    <button type='submit' disabled={props.isSubmitting}>Submit</button>
                </div>
            </Form>
        </Formik>
    )
}

export default MyForm

