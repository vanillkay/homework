import React from 'react'
import { useField } from 'formik';

function Input(props) {
    const { label, name, ...rest } = props;

    const [field, meta, helpers] = useField(name);

    return (
        <>
            <div>
                <label>{label}
                    <input {...field} {...rest} />
                </label>
            </div>
            {meta.touched && meta.error && <div className='error'>{meta.error}</div>}
        </>
    )
}

export default Input
