import React, {Component} from 'react';
import Button from "../Button/Button";
import './Product.scss';
import Star from "../Star/Star";
import PropTypes from 'prop-types';


class Product extends Component {
    render() {
        const {name, price, path, article, color, toggleLovelyProduct, toggleModalWindow, isLovely} = this.props;
        const isInBin = JSON.parse(localStorage.getItem('productsInBin')).includes(article);
        return (
            <div className='product-card'>
                <Star productArticle={article} isLovely={!!isLovely} toggleLovelyProduct={toggleLovelyProduct}/>
                <div className='product-card__photo'>
                    <img src={path} alt=""/>
                </div>
                <h2 className='product-card__name'>{name}</h2>
                <div className='product-card__description'>
                    <p className='product-card__text'>Product description</p>
                    <p className='product-card__color'>Color: {color}</p>
                </div>
                <div className="product-card__price-block">
                    <div className="product-card__price">Price: {price}</div>
                    <Button action={() => toggleModalWindow({name, price, color, article})} backgroundCol={isInBin ? 'green': ''} text={isInBin ? 'IN BIN' : 'ADD TO BIN'}/>
                </div>
            </div>
        );
    }
}

Product.propTypes = {
    name: PropTypes.string,
    price: PropTypes.string,
    path: PropTypes.string,
    article: PropTypes.number,
    color: PropTypes.string,
    toggleModalWindow: PropTypes.func.isRequired,
    toggleLovelyProduct: PropTypes.func.isRequired,
    isLovely: PropTypes.bool
}

Product.deafultProps = {
    name: 'product',
    price: 'no price',
    path: '',
    article: 'no article',
    color: 'no color',
    isLovely: false
}
export default Product;