import React, {Component} from 'react';
import './ModalWindow.scss';
import PropTypes from 'prop-types';
import Button from "../Button/Button";

class ModalWindow extends Component {
    static keys = {37: 1, 38: 1, 39: 1, 40: 1};
    render() {
        const {modalInfo, toggleModalWindow, toggleBin} = this.props;
        const {name, price, color, article} = modalInfo || {name: 'product', price: 'no price', color: ''};
        const isInBin = JSON.parse(localStorage.getItem('productsInBin')).includes(article);
        return (
            <>
                <div className="modal__window">
                    <div className='modal'>
                        <div className="modal__header">
                            <div className="modal__title">{isInBin ? `${color} ${name} now in bin` : `Are you sure to buy this ${name} ?`}</div>
                            <Button action={toggleModalWindow} text='X' backgroundCol='black'/>
                        </div>
                        <div className="modal__content">{color} {name}, price - {price}</div>
                        <div className="modal__actions"><Button action={() => {toggleBin(article)} } backgroundCol={isInBin ? 'green': ''} text={isInBin ? 'DELETE FROM BIN ?' : 'BUY'}/></div>
                    </div>
                </div>
            </>
        );
    }

    preventDefault = (e) => {
        e.preventDefault();
    }

    preventDefaultForScrollKeys = (e) => {
        if (ModalWindow.keys[e.keyCode]) {
            this.preventDefault(e);
            return false;
        }
    }

    componentDidMount() {
        let supportsPassive = false;
        try {
            window.addEventListener("test", null, Object.defineProperty({}, 'passive', {
                get: function () { supportsPassive = true;}
            }));
        } catch(e) {}

        this.wheelOpt = supportsPassive ? { passive: false } : false;
        this.wheelEvent = 'onwheel' in document.createElement('div') ? 'wheel' : 'mousewheel';
        this.disableScroll();
    }

    componentWillUnmount() {
        this.enableScroll();
    }

    disableScroll = () => {
        window.addEventListener('DOMMouseScroll', this.preventDefault, false);
        window.addEventListener(this.wheelEvent, this.preventDefault, this.wheelOpt);
        window.addEventListener('touchmove', this.preventDefault, this.wheelOpt);
        window.addEventListener('keydown', this.preventDefaultForScrollKeys, false);
    }

// call this to Enable
     enableScroll = () => {
        window.removeEventListener('DOMMouseScroll', this.preventDefault, false);
        window.removeEventListener(this.wheelEvent, this.preventDefault, this.wheelOpt);
        window.removeEventListener('touchmove', this.preventDefault, this.wheelOpt);
        window.removeEventListener('keydown', this.preventDefaultForScrollKeys, false);
    }
}

ModalWindow.propTypes = {
    modalInfo: PropTypes.object,
    toggleModalWindow: PropTypes.func.isRequired,
    toggleBin: PropTypes.func.isRequired
}

ModalWindow.defaultProps = {
    modalInfo: {}
}
export default ModalWindow;