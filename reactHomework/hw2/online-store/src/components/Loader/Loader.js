import React, {Component} from 'react';
import './Loader.scss'

class Loader extends Component {
    render() {
        return (
            <div className='loader'>Loading...</div>
        );
    }
}

export default Loader;