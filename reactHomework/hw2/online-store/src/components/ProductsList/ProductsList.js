import React, {Component} from 'react';
import Product from "../Product/Product";
import PropTypes from 'prop-types';
import './ProductsList.scss';

class ProductsList extends Component {
    render() {
        const {products, lovelyProducts, toggleLovelyProduct, toggleModalWindow} = this.props;
        const productsCards = products.map(item => {
            const isLovely = lovelyProducts.includes(item.article);
            return <Product isLovely={isLovely} key={item.article} toggleModalWindow={toggleModalWindow} toggleLovelyProduct={toggleLovelyProduct} {...item}/>
        });
        return (
            <div className="container">
                <div className="products__title">Our products</div>
                <div className='products'>
                    {productsCards};
                </div>
            </div>
        );
    }
}

ProductsList.propTypes = {
    products: PropTypes.array,
    lovelyProducts: PropTypes.array,
    toggleModalWindow: PropTypes.func.isRequired,
    toggleLovelyProduct: PropTypes.func.isRequired
}

ProductsList.defaultProps = {
    products: [],
    lovelyProducts: []
}

export default ProductsList;