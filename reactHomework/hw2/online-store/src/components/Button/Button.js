import React, {Component} from 'react';
import './Button.scss';
import PropTypes from 'prop-types';


class Button extends Component {
    render() {
        const {backgroundCol, text, action} = this.props;
        return (
            <button  onClick={() => action()} style={{backgroundColor: backgroundCol}} className='button'>{text}</button>
        );
    }
}


Button.propTypes = {
    backgroundCol: PropTypes.string,
    text: PropTypes.string,
    action: PropTypes.func.isRequired
}

Button.defaultProps = {
    backgroundCol: 'black',
    text: 'Push me'
}

export default Button;