import React, {Component} from 'react';
import './Star.scss'
import PropTypes from 'prop-types';

class Star extends Component {
    render() {
        const {isLovely, toggleLovelyProduct, productArticle} = this.props;
        return (
            <>
                {!isLovely && <i id='star' onClick={() => toggleLovelyProduct(productArticle)} className="far fa-star"></i>}
                {isLovely && <i id='star-active' onClick={() => toggleLovelyProduct(productArticle)} className="fas fa-star"></i>}
            </>
        );
    }
}

Star.propTypes = {
    isLovely: PropTypes.bool,
    toggleLovelyProduct: PropTypes.func.isRequired,
    productArticle: PropTypes.number
}

Star.defaultProps = {
    isLovely: false,
    productArticle: ''
}

export default Star;