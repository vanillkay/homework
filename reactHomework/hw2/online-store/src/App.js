import './App.css';
import React, {Component} from 'react';
import Loader from "./components/Loader/Loader";
import axios from 'axios';
import ProductsList from "./components/ProductsList/ProductsList";
import ModalWindow from "./components/ModalWindow/ModalWindow";


class App extends Component {
    state = {
        products: [],
        isLoading: true,
        lovelyProducts: [],
        productsInBin: [],
        isModal: false,
        modalInfo: {}
    }

    render() {
        const {isLoading, products, lovelyProducts, isModal, modalInfo} = this.state;
        return (
            <div>
                {isLoading ? <Loader/> : <ProductsList toggleModalWindow={this.toggleModalWindow} toggleLovelyProduct={this.toggleLovelyProduct} lovelyProducts={lovelyProducts} products={products}/>}
                {isModal && <ModalWindow toggleBin={this.toddleBin} toggleModalWindow={this.toggleModalWindow} modalInfo={modalInfo}/>}
            </div>
        )
    }

    componentDidMount() {
        let {lovelyProducts, productsInBin} = this.state;
        if (localStorage.getItem('lovelyProducts') === null){
            localStorage.setItem('lovelyProducts', JSON.stringify(lovelyProducts));
        }else{
            lovelyProducts = JSON.parse(localStorage.getItem('lovelyProducts'));
        }

        if (localStorage.getItem('productsInBin') === null){
            localStorage.setItem('productsInBin', JSON.stringify(productsInBin));
        }else{
            productsInBin = JSON.parse(localStorage.getItem('productsInBin'));
        }
        this.setState({...this.state, lovelyProducts: lovelyProducts, productsInBin: productsInBin})
        axios('/products/product-list.json')
            .then(response => this.setState({...this.state, products: response.data, isLoading: false}));
    }

    toggleLovelyProduct = (prodArticle) => {
        const {lovelyProducts} = this.state;
        let newLovelyProducts;
        if (lovelyProducts.includes(prodArticle)){
            const position = lovelyProducts.indexOf(prodArticle);
            lovelyProducts.splice(position, 1);
            newLovelyProducts = [...lovelyProducts];
        }else{
            newLovelyProducts = [...lovelyProducts, prodArticle];
        }
        localStorage.setItem('lovelyProducts', JSON.stringify(newLovelyProducts));
        this.setState({...this.state, lovelyProducts: newLovelyProducts});
    }

    toggleModalWindow = (modalParams) => {
        const {isModal} = this.state;
        const modalInfo = isModal ? {} : modalParams;
        this.setState({...this.state, isModal: !isModal, modalInfo: modalInfo});
    }

    toddleBin = (prodArticle) => {
        const {productsInBin} = this.state;
        let newProductsInBin;
        if (productsInBin.includes(prodArticle)){
            const position = productsInBin.indexOf(prodArticle);
            productsInBin.splice(position, 1);
            newProductsInBin = [...productsInBin];
        }else{
            newProductsInBin = [...productsInBin, prodArticle];
        }
        localStorage.setItem('productsInBin', JSON.stringify(newProductsInBin));
        this.setState({...this.state, isModal: !this.state.isModal, modalInfo: {}, productsInBin: newProductsInBin});
    }
}

export default App;
