export const getIsModalSelector = ({modalWindow}) => modalWindow.isModal;
export const getModalInfoSelector = ({modalWindow}) => modalWindow.modalInfo;