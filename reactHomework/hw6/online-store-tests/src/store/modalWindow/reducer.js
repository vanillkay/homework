import * as modalActionTypes from './types';

const initialState = {
    isModal: false,
    modalInfo: {},
}

const reducer = (state = initialState, action ) => {
    switch(action.type){
        case modalActionTypes.SET_IS_MODAL:
            return {...state, isModal: action.payload};
        case modalActionTypes.SET_MODAL_INFO:
            return {...state, modalInfo: action.payload};
        case modalActionTypes.TOGGLE_MODAL_WINDOW:
            const modalInfo = state.isModal ? {} : action.payload;
            return {...state, isModal: !state.isModal, modalInfo: modalInfo};
        default: return state;
    }
}

export default reducer;