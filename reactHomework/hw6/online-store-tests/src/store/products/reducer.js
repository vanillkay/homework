import * as productsActionTypes from './types';
import {toggleLovelyProduct, toggleProductsInCart, localStorageCheck} from "./operations";

const initialState = {
    products: [],
    isLoading: true,
    favoritesProducts: localStorageCheck('lovelyProducts'),
    productsInCart: localStorageCheck('productsInBin'),
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case productsActionTypes.PRODUCTS_LOADING:
            return {...state, isLoading: action.payload};
        case productsActionTypes.SAVE_PRODUCTS:
            return {...state, products: action.payload};
        case productsActionTypes.TOGGLE_FAVORITES:
            const newFavoritesProducts = toggleLovelyProduct(state.favoritesProducts, action.payload);
            return {...state, favoritesProducts: newFavoritesProducts};
        case productsActionTypes.TOGGLE_PRODUCTS_IN_CART:
            const newProductsInCart = toggleProductsInCart(state.productsInCart, action.payload);
            return {...state, productsInCart: newProductsInCart};
        case productsActionTypes.CLEAR_BIN:
            return {...state, productsInCart: []}
        default: return state;
    }
};

export default reducer;