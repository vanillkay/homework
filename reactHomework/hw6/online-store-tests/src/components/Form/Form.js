import React from 'react';
import {Form, withFormik} from 'formik';
import Input from './Input/Input';
import * as yup from 'yup';
import * as productsActions from "../../store/products/actions";
import {useDispatch, connect} from 'react-redux';

const FIELD_REQUIRED = 'This field is required!!!'
const schema = yup.object().shape({
    name: yup
        .string()
        .required(FIELD_REQUIRED)
        .email('This is not a valid name!!!'),
    surname: yup
        .string()
        .required(FIELD_REQUIRED)
        .email('This is not a valid surname!!!'),
    age: yup
        .number()
        .required(FIELD_REQUIRED),
    delivery: yup
        .string()
        .required(FIELD_REQUIRED)
        .email('This is not a place for delivery!!!'),
    phone: yup
        .number().required(FIELD_REQUIRED)

});


// const toggleBin = () => dispatch(productsActions.clearBinAction());

const handleSubmit = (values, {setSubmitting}) => {
    setTimeout(() => {
        setSubmitting(false)
    }, 1000)
}

const MyForm = (props) => {
    // const dispatch = useDispatch();


    return (
        <Form className='login' noValidate>
            <Input label='name' type='text' placeholder='name' name='name'/>
            <Input label='surname' type='text' placeholder='surname' name='surname'/>
            <Input label='age' type='text' placeholder='your age' name='age'/>
            <Input label='Delivery place' type='text' placeholder='your delivery place' name='delivery'/>
            <Input label='phone' type='text' placeholder='your phone' name='phone'/>
            <div>
                <button type='submit' disabled={props.isSubmitting}>Submit</button>
            </div>
        </Form>
    )
}

export default withFormik({
    mapPropsToValues: (props) => ({
        name: '',
        surname: '',
        age: '',
        delivery: '',
        phone: ''
    }),
    handleSubmit,
    validationSchema: schema
})(connect()(MyForm))

