import React from 'react';
import {unmountComponentAtNode, render} from 'react-dom';
import {act} from 'react-dom/test-utils';
import ModalWindow from './ModalWindow';


let container = null;

beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
})

afterEach(() => {
    unmountComponentAtNode(container);
    container.remove()
    container = null
})


describe('Testing ModalWindow.js', () => {
    test('Smoke test of ModalWindow.js', () => {
        act(() => {
            const testFn = jest.fn()
            render(<ModalWindow toggleBin={testFn} toggleModalWindow={testFn}/>, container)
        })
    })

    test('When modalInfo props is undefined components tender a default values', () => {
            act(() => {
                const testFn = jest.fn()
                render(<ModalWindow toggleBin={testFn} toggleModalWindow={testFn}/>, container)
            })
            const text = document.getElementsByClassName('modal__content')[0];
            expect(text.textContent).toContain('product')
            expect(text.textContent).toContain('no price')
        }
    )

    test('When product in bin addToBin button changes', () => {
            act(() => {
                const testFn = jest.fn()
                render(<ModalWindow productsInBin={[123]} modalInfo={{article: 123}} toggleBin={testFn}
                                    toggleModalWindow={testFn}/>, container)
            })
            const button = document.querySelector('.modal__actions > button');
            expect(button.textContent).toBe('DELETE FROM CART ?');
            expect(button.style.backgroundColor).toBe('green');
        }
    )

    test('When product in bin addToBin title changes', () => {
            act(() => {
                const testFn = jest.fn()
                render(<ModalWindow productsInBin={[123]} modalInfo={{article: 123}} toggleBin={testFn}
                                    toggleModalWindow={testFn}/>, container)
            })
            const title = document.querySelector('.modal__title');
            expect(title.textContent).toContain('now in cart');

        }
    )

    test('When props received a component it will be rendered in the modal window', () => {
            act(() => {
                const testFn = jest.fn();
                const testComponent = <div className={'test'}>test</div>
                render(<ModalWindow productsInBin={[123]} modalInfo={{article: 123}} toggleBin={testFn}
                                    toggleModalWindow={testFn} component={testComponent}/>, container)
            })

            const modalContent = document.querySelector('.modal__content');
            console.log(modalContent.innerHTML)

            expect(modalContent.innerHTML).toBe("<div class=\"test\">test</div>");

        }
    )


});