import React from 'react';
import {unmountComponentAtNode, render} from 'react-dom';
import {act} from 'react-dom/test-utils';
import Button from './Button';

let container = null;

beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
})

afterEach(() => {
    unmountComponentAtNode(container);
    container.remove()
    container = null
})


describe('Testing Button.js', () => {
    test('Smoke test of Button.js', () => {
        act(() => {
            render(<Button/>, container)
        })
    })

    test('When the props text is true button contain this text', () => {
        act(() => {
            const testText = 'test'
            render(<Button text={testText}/>, container);
            const button = document.getElementsByTagName('button')[0];
            expect(button.textContent).toBe(testText.toUpperCase());
        })
    })

    test('When the props text is missing button contain this default text', () => {
        act(() => {
            const defaultText = 'PUSH ME'
            render(<Button/>, container);
            const button = document.getElementsByTagName('button')[0];
            expect(button.textContent).toBe(defaultText);
        })
    })

    test('Call a props function', () => {
        act(() => {
            const testFunc = jest.fn()
            render(<Button action={testFunc}/>, container);
            const button = document.getElementsByTagName('button')[0];
            button.click()
            expect(testFunc).toHaveBeenCalled()
        })
    })
    test('Have background style when it comes from props', () => {
        act(() => {
            const bgTest = 'red';
            render(<Button backgroundCol={bgTest}/>, container);
            const button = document.getElementsByTagName('button')[0];
            expect(button.style.backgroundColor).toBe(bgTest)
        })
    })

    test('Have a default black background when it does not comes from props', () => {
        act(() => {
            render(<Button />, container);
            const button = document.getElementsByTagName('button')[0];
            expect(button.style.backgroundColor).toBe('black')
        })
    })


});

