import React, {Component} from 'react';
import {createUseStyles} from "react-jss";
import './Button.scss';

const Button = (props) => {
    const {action, backgroundCol, text} = props;
    return (
        <button onClick={action} style={{backgroundColor: backgroundCol}}
                className='button'>{text || 'No text'}</button>
    );
}

export default Button;