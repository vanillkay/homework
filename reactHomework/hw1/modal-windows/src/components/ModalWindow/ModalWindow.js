import React from 'react';
import Button from "../Button/Button";
import {createUseStyles} from "react-jss";


const useStyles = createUseStyles({
    modal: {
        display: 'flex',
        padding: [[20, 30,]],
        flexDirection: 'column',
        justifyContent: 'space-between',
        position: 'absolute',
        backgroundColor: 'lightblue',
        width: '50vw',
        height: '40vh',
    },
    modal__header: {
        width: '100%',
        height: 'max-content',
        display: 'inline-flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    modal__title: {
        font:{
            size: '30pt',
            weight: 'bold',
        }
    },
    modal__content: {
        margin: [[20, 0]],
        fontSize: '20pt'
    },
    modal__actions: {
        display: 'flex',
        justifyContent: 'center'
    },

    '@media screen and (max-width: 720px)': {
        modal: {
            backgroundColor: 'black'
        }
    }
});

const ModalWindow = (props) => {
    const {header, text, actions, close, closeAction} = props;
    const classes = useStyles();
    console.log(classes);
    return (
        <div className={classes.modal}>
            <div className={classes.modal__header}>
                <div className={classes.modal__title}>{header}</div>
                {close && <Button action={closeAction} styles={{marginRight: '0px'}} text='X'
                                  backgroundCol='black'/>}
            </div>
            <div className={classes.modal__content}>{text}</div>
            <div className={classes.modal__actions}>{actions}</div>
        </div>
    );
}

export default ModalWindow;