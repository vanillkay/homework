import './App.css';
import {createUseStyles} from "react-jss";

import React, {useState, Component} from 'react';
import Button from "./components/Button/Button";
import ModalWindow from "./components/ModalWindow/ModalWindow";

class App extends Component {
    state = {
        isModalOpen: {
            firstModal: false,
            secondModal: false,
        }
    }

    render() {
        const {isModalOpen} = this.state;
        const {firstModal, secondModal} = isModalOpen;

        if (firstModal || secondModal) document.body.classList.add('hidden');
        document.body.addEventListener('click', (event) => {
            if ((this.state.isModalOpen.firstModal || this.state.isModalOpen.secondModal) && event.target.className === 'App') {
                this.setState({...this.state, isModalOpen: {firstModal: false, secondModal: false}})
                document.body.classList.remove('hidden');
            }
        })

        return (
            <div className='App'>
                <Button backgroundCol='#E74C3C' text="Open first modal"
                        action={() => this.closeOrOpenWindow('firstModal')}/>
                <Button backgroundCol='#E74C3C' text="Open second modal"
                        action={() => this.closeOrOpenWindow('secondModal')}/>
                {firstModal && <ModalWindow
                    header='Modal window first'
                    text='Hello from the first modal'
                    actions={[
                        <Button key={1} action={() => alert('Hello from action1 in the first modal')}
                                text='Action1 in the first modal' backgroundCol='black'/>,
                        <Button key={2} action={() => alert('Hello from action2 in the first modal')}
                                text='Action2 in the first modal' backgroundCol='black'/>
                    ]}
                    closeAction={() => this.closeOrOpenWindow('firstModal')}
                    close/>}
                {secondModal && <ModalWindow
                    header='Modal window second'
                    text='Hello from the second modal'
                    actions={[
                        <Button key={1} text='Action1 in the second modal' backgroundCol='black'
                                action={() => alert('Hello from action1 in the second modal')}/>,
                        <Button key={2} text='Action2 in the second modal' backgroundCol='black'
                                action={() => alert('Hello from action2 in the second modal')}/>
                    ]}
                    closeAction={() => this.closeOrOpenWindow('secondModal')}
                    close/>}
            </div>
        );
    }

    closeOrOpenWindow = (window) => {
        this.setState({...this.state, isModalOpen: {[window]: !this.state.isModalOpen[window]}});
        if (document.body.classList.contains('hidden')) document.body.classList.remove('hidden');

    }
}

export default App;
