
    function filterBy(arr, key) {
        return [...arr].filter( item => typeof item !== key);
    }
    let arTest = ['hello', 'world', 23, '23', null];
    let newArr = filterBy(arTest, 'string');
    console.log(newArr);