const images = document.querySelectorAll('.image-to-show');
images.forEach(item => {item.style.display = 'none'});
const stopButton = document.createElement('input');
stopButton.type = 'button';
stopButton.value = 'STOP';
stopButton.style.margin = '20px';
const continueButton = document.createElement('input');
continueButton.type = 'button';
continueButton.value = 'CONTINUE';
document.querySelector('.images-wrapper').insertAdjacentElement('afterend', stopButton);
stopButton.insertAdjacentElement('afterend', continueButton);
let showImages;
const timeElem = document.createElement('div');
continueButton.insertAdjacentElement('afterend', timeElem);

function cycledShow(images, counter) {
    showImages = setTimeout(function show(images, counter) {
        if (counter === 4) {
            fadeOut(images[3]);
            images[3].style.cssText = 'display: none;';
            counter = 0;
        }
        if (counter) {
            fadeOut(images[counter-1]);
            images[counter - 1].style.cssText = 'display: none;';
        }
        let countdown = new Date();
        let responseTime = new Date(Date.now() + (1000*10)); // таймер 10 секунд

        function startTime() {
            countdown.setTime(responseTime - Date.now());
            timeElem.innerHTML = 'Next picture in –> ' + countdown.getUTCSeconds() + ':' + countdown.getUTCMilliseconds() + ' seconds';
            requestAnimationFrame(startTime);
        }
        requestAnimationFrame(startTime);
        fadeIn(images[counter], 500);
        showImages = setTimeout(show, 10000, images, counter + 1)
    }, 0, images, counter);
}
cycledShow(images, 0);

document.body.addEventListener('click', function (event) {
if (event.target.value === 'STOP'){
    clearTimeout(showImages);
    timeElem.style.display = 'none';
    console.log('stop')
}
if (event.target.value === 'CONTINUE'){
    timeElem.style.display = 'block';
    let counter;
    images.forEach((item, index) => {
        if (item.style.display === 'block') counter = index;
    });
    cycledShow(images, counter);
}
});

function fadeIn(el, time) {
    el.style.display = 'block';
    el.style.opacity = '0';
    let last = +new Date();
    const tick = function() {
        el.style.opacity = +el.style.opacity + (new Date() - last) / time;
        last = +new Date();
        if (+el.style.opacity < 1) (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
    };
    tick();
}


function fadeOut(el){
    el.style.opacity=1;
    (function fade(){
        if((el.style.opacity-=.1)<0){
            el.style.display="none";
        }else{
            requestAnimationFrame(fade);
        }
    })();
}





