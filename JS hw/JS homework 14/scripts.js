$(document).ready(() => {
    const btnToTop = $('.scroll-top');
    const checkScrollingViewPort = (event) => {
        const scrolling = window.scrollY;
        const windowHeight = window.innerHeight;
        if (scrolling > windowHeight) {
            btnToTop.fadeIn(100);
        } else {
            btnToTop.fadeOut(100);
        }
    };
    checkScrollingViewPort();

    $(window).scroll(checkScrollingViewPort);

    btnToTop.on('click', () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth',
        });
    });
    const contentNav = $('.contentNav');
    contentNav.on('click', function (event) {
        if (event.target.tagName !== 'A') return;
        const elementClick = $(event.target).attr("href");
        const destination = $(elementClick).offset().top;
        window.scrollTo({
            top: destination,
            behavior: 'smooth',
        });
        return false;
    });
    const slider = $('.sliderToggle');
    slider.on('click', () => {
        $('.mainContent-Photo').slideToggle("slow");
    })
});

