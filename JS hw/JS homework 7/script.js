
function makeList(elem) {
    const list = document.createElement('ul');
    let  arrList = [];
    if (Array.isArray(elem)){
        arrList = [...elem.map(item => {
            const elem = document.createElement('li');
            if (Array.isArray(item) || typeof item === 'object') {
                elem.append(makeList(item));
            }else{
                elem.innerText = item;
            }
            return elem;
        })];
    }else{
        for (let key in elem){
            const listElem = document.createElement('li');
            if (Array.isArray(elem[key]) || typeof elem[key] === 'object'){
              listElem.append(makeList(elem[key]));
            }else {
                listElem.innerText = elem[key];
            }
            arrList.push(listElem);
        }
    }
    list.append(...arrList);
    return list;
}
function showList(arr) {
    const list = makeList(arr);
    document.body.append(list);
    const timerBlock = document.createElement('div');
    timerBlock.style.cssText =  `width: 300px; 
                                 height: 100px;
                                 background-color:black;
                                 color: white;
                                 display: flex;
                                 align-items:center;
                                 justify-content: center;
                                 font-size: 25px;
                                 text-align: center;
                                 border-radius: 10px;`;
    document.body.append(timerBlock);
    let seconds = 10;
    const timerPage = setInterval(() => {
        timerBlock.innerText = `The page will be updated in \n${seconds}`;
        seconds--;
        } , 1000);
    setTimeout(() => {
        clearInterval(timerPage);
        timerBlock.innerText = "UPDATE!!!";
        setTimeout(() => document.body.innerHTML = "", 1000)}, 12000);
}

showList(['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']);