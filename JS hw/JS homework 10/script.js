const eyes = document.querySelectorAll('i');
const firstPasswordField = document.querySelectorAll('input')[0];
const secondPasswordField = document.querySelectorAll('input')[1];
const warning = document.createElement('span');
warning.innerText = 'Нужно ввести одинаковые значения!!!';
warning.style.color = 'red';
eyes[0].addEventListener('click', (event) => {
        event.target.classList.toggle('fa-eye-slash');
        if (firstPasswordField.type === 'text'){
            firstPasswordField.type = 'password';
        }else{
            firstPasswordField.type = 'text';
        }
    });
eyes[0].classList.toggle('fa-eye-slash');
eyes[1].addEventListener('click', function(event) {
    if (this.classList.contains('fa-eye-slash')){
        this.className = 'fas fa-eye icon-password';
    }else {
        this.className = 'fas fa-eye-slash icon-password';
    }
    if (secondPasswordField.type === 'text'){
        secondPasswordField.type = 'password';
    }else{
        secondPasswordField.type = 'text';
    }
});

const submitButton = document.querySelector('.btn');
submitButton.addEventListener('click', function (event) {
    warning.remove();
    if (firstPasswordField.value === secondPasswordField.value){
        alert("You are welcome!!!");
        firstPasswordField.value = secondPasswordField.value = '';
    }else{
        secondPasswordField.value = '';
        secondPasswordField.closest('label').insertAdjacentElement('afterend',warning);
    }
    return false;
});