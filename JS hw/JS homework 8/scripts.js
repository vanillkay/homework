const priceTExt = document.createElement('span');
const errorText = document.createElement('span');
errorText.style.visibility = 'hidden';
errorText.style.fontSize = '18px';
errorText.innerText = 'Please enter correct price';
document.getElementsByClassName('enter')[0].insertAdjacentElement('afterend',errorText);
priceTExt.style.visibility = 'hidden';
priceTExt.style.marginBottom = '10px';
priceTExt.style.fontSize = '18px';
priceTExt.innerText = "Текущая цена:";
document.querySelector('div').prepend(priceTExt);
const inputEl = document.querySelector('input');
const button = document.querySelector('button');
inputEl.addEventListener('focus', function () {this.style.border = '4px solid green';
});
inputEl.addEventListener('blur', function () {
    const price = this.value;
    if (price < 0 ){
        button.style.visibility = 'visible';
        inputEl.style.border = '4px solid red';
        errorText.style.visibility = 'visible';
        priceTExt.style.visibility = 'hidden';
    }else {
        button.style.visibility = 'visible';
        this.style.border = 'none';
        this.style.color = 'green';
        priceTExt.style.visibility = 'visible';
        priceTExt.innerText = "Текущая цена: " + this.value + '$';
        errorText.style.visibility = 'hidden';
    }
});
button.addEventListener('click', function () {
    inputEl.style.border = 'none';
    inputEl.value = "";
    this.style.visibility = 'hidden';
    errorText.style.visibility = 'hidden';
    priceTExt.style.visibility = 'hidden';
});

