const getNum = function () {
    let number =  parseInt(prompt("Enter the number"));
    while (isNaN(number)){
        number =  parseInt(prompt("Enter the number"));
    }
    return number;
};
const getFact = function (num) {
    //Базовый случай
    if (num == 1){
        return 1;
    }
    //Рекурсивный случай
    return num*getFact(num -1);
};


let number= getNum();
let res = getFact(number);
document.write(`${res}`);