function getRandomColor() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);
    return `rgb(${r}, ${g}, ${b})`;
}

const drawCircleButton = document.querySelector('input');
drawCircleButton.addEventListener('click', () => {
    drawCircleButton.parentElement.nextElementSibling.remove();
    const drawMenu = document.createElement('div');
    const enterDiameter = document.createElement('input');
    const drawButton = document.createElement('input');
    enterDiameter.type = 'number';
    drawButton.type = 'button';
    drawButton.value = 'DRAW';
    drawButton.addEventListener('click', (event) => {
        const diameter = enterDiameter.value;
        enterDiameter.remove();
        drawButton.remove();
        let circles = new DocumentFragment();
        for (let i = 0; i < 100; i++){
            const oneCircle = document.createElement('div');
            oneCircle.style.cssText = `margin: 5px; width: ${diameter}px; height: ${diameter}px; border-radius: ${diameter/2}px; background: ${getRandomColor()}; display: inline-block;`;
            circles.append(oneCircle);
        }
        drawMenu.append(circles);
        drawMenu.addEventListener('click', (event) => {
            event.target.style.display = 'none';
        })
    });
    drawMenu.insertAdjacentElement('afterbegin', enterDiameter);
    drawMenu.insertAdjacentElement('beforeend', drawButton);
    drawCircleButton.parentElement.insertAdjacentElement('afterend', drawMenu);
});