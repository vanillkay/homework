
function createNewUser() {
    let name = prompt("Enter your name, please");
    while (!(isNaN(+name))){
        name = prompt("Enter your name, please");
    }
    let surName = prompt("Enter your surname, please");
    while (!(isNaN(+surName))){
        surName = prompt("Enter your surname, please");
    }
    let birthday = prompt("Enter your birthday(date.month.year)");
    let obj =  {
        name: name,
        surName: surName,
        birthday: new Date(birthday.split(".").reverse().join(".")),
        getAge(){
            let todayDate = new Date();
            let age = todayDate.getFullYear()-this.birthday.getFullYear();
            if (todayDate.getMonth()<this.birthday.getMonth() ) return `${this.name} ${this.surName}, your age is ${age-1} years old`;
            if (todayDate.getMonth() === this.birthday.getMonth() && todayDate.getDate()<this.birthday.getDate()) return `${this.name} ${this.surName}, your age is ${age-1} years old`;
            return `${this.name} ${this.surName}, your age is ${age} years old`;
        },
        getPassword(){
          return this.name[0].toUpperCase()+this.surName.toLowerCase()+this.birthday.getFullYear();
        },
        getLogin(){
            return (this.name[0]+this.surName).toLowerCase();
        },
        setName(newName){
            Object.defineProperties(this,{
            name:{
                writable: true,
            }
            });
            this.name = newName;
            Object.defineProperties(this,{
                name:{
                    writable: false,
                }
            });
        },
        setSurName(newSurName){
            Object.defineProperties(this,{
                surName:{
                    writable: true,
                }
            });
            this.surName = newSurName;
            Object.defineProperties(this,{
                surName:{
                    writable: false,
                }
            });
        },
    };
    Object.defineProperties(obj,{
       name:{
           writable: false,
       } ,
        surName:{
           writable: false,
        },
    });
    return obj;
}



let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());

