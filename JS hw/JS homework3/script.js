const getNumber = function () {
    let number =  parseFloat(prompt("Enter the number"));
    while (isNaN(number)){
        number =  parseFloat(prompt("Enter the number"));
    }
    return number;
};

const getSIgn = function () {
    return prompt("Enter the sign");
};

const calcRes = function (number1, number2, sign) {
    let res;
    switch (sign) {
        case "+":
            res = calcSum(number1, number2);
            break;
        case "-":
            res = calcDiff(number1, number2);
            break;
        case "/":
            res = calcDiv(number1, number2);
            break;
        case "*":
            res = calcMult(number1, number2);
            break;
    }
    return res;
};

const calcSum = function (num1, num2) {
    return num1 + num2;
};

const calcDiff = function (num1, num2) {
    return num1 - num2;
};
const calcDiv = function (num1, num2) {
    return num1/num2;
};
const calcMult = function (num1, num2) {
    return num1 * num2;
};

const showRes = function (number1, number2, sign, result) {
    console.log(`The ${sign} of numbers: ${number1} and ${number2} is ${result}`);
};

const showError =function () {
    return "Mistake!!!We cant divide by zero!!!";
};

const checkDiv = function (number, sign) {
    if (number == 0 && sign == "/"){
        return showError;
    }else {
        return calcRes;
    }
};

const mathFunction = function () {
    let firstNumber = getNumber();
    let secondNumber = getNumber();
    let sign = getSIgn();
    let res = checkDiv(secondNumber, sign)(firstNumber,secondNumber,sign);
    showRes(firstNumber, secondNumber, sign, res);
};

mathFunction();