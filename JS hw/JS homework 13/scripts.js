document.addEventListener('DOMContentLoaded', function () {
    const changeButton = document.querySelector('input');
    const header = document.querySelector('header');
    const elemsToChange = [
        ...document.querySelectorAll('h1'),
        ...document.querySelectorAll('h2'),
        ...document.querySelectorAll('p'),
        ...document.querySelectorAll('a'),
        ...document.querySelectorAll('div')
    ];
    
    function changeColor(elem, changer){
        elem.style.color = `${changer}`;
    }

    function check(key) {
        if (!localStorage.getItem(`${key}`)) return;
        elemsToChange.forEach(item => {changeColor(item, 'red')});
        header.style.backgroundColor = 'black';
    }
    check('THEME');

    changeButton.addEventListener('click', function () {
        if (!localStorage.getItem('THEME')) {
            elemsToChange.forEach(item => {changeColor(item, 'red')});
            header.style.backgroundColor = 'black';
            localStorage.setItem('THEME', 'red');
        } else{
            localStorage.clear();
            elemsToChange.forEach(item => {changeColor(item, '')});
            header.style.backgroundColor = '';
        }
    })

});