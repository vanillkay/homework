const listElems = document.getElementsByClassName('tabs-content')[0].children;
const buttonEl = document.getElementsByClassName('tabs')[0].children;
const elem = document.getElementsByClassName('tabs')[0];
for (let i = 0; i < listElems.length; i++){
    listElems[i].style.display = "none";
}
elem.addEventListener('click', (event) => {
    for (let i = 0; i < buttonEl.length; i++){
            buttonEl[i].classList.remove('active');
        }
        event.target.classList.add('active');
        for (let i = 0; i < listElems.length; i++){
            listElems[i].style.display = "none";
        }
        [...listElems].forEach(item => {
            if (item.dataset.name === event.target.innerText){
                item.style.display = "list-item";
            }
        })
});
buttonEl[0].click();