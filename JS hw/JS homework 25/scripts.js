document.addEventListener('DOMContentLoaded', function () {
    const images = document.querySelectorAll('img');
    images.forEach(item => item.style.display = 'none');
    images[0].style.display = 'block';

    function moveImages(images, direction) {
        for (let i = 0; i < images.length; i++) {
            if (images[i].style.display === 'block') {
                images[i].style.display = 'none';
                switch (direction) {
                    case 'PREVIOUS': {
                        if (images[i].dataset.position === '0') {
                            images[5].style.display = 'block';
                           return;
                        }
                        images[i - 1].style.display = 'block';
                    }
                        break;
                    case 'NEXT': {
                        if (images[i].dataset.position === '5') {
                            images[0].style.display = 'block';
                            return;
                        }
                        images[i + 1].style.display = 'block';
                        return;
                    }

                }
            }
        }
    }

    document.addEventListener('click', function (event){
        const targetParent = event.target.parentElement;
        if (!targetParent) return;
        if (targetParent.classList.contains('buttons')){
            moveImages(images, event.target.value);
        }

    });
});
