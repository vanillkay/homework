let workers = [5,2,6];
let tasks = [11,5,15];
let deadline= new Date(2020,4,22);

function getDeadline(workerQuality, tasks, deadline) {
    let startDate = new Date();
    let workTogetherSkill=0;
    let workingDays;
    let allTask=0;
    let workingResult;
    //Считаю общую продуктивность
    for (let hoursOfWork of workerQuality){
        workTogetherSkill+=hoursOfWork;
    }
    //Считаю общий объём задачи
    for (let task of tasks){
        allTask+=task;
    }
    //Узнаю сколько нужно дней на выполнение
    workingResult=Math.ceil(allTask/workTogetherSkill);
    //Считаю точное количество дней до дедлайна с учётом выходных
    for (let i = 0; ; i++){
        if (startDate.getDate() === deadline.getDate()){
            workingDays = i;
            if (startDate.getDay() === 0 || startDate.getDay() === 6) workingDays -=1;
            break;
        }
        if (startDate.getDay() === 0 || startDate.getDay() === 6 ) {
            startDate.setDate(startDate.getDate()+1);
            i--;
            continue;
        }
        startDate.setDate(startDate.getDate()+1);
    }
    if (workingResult <=workingDays) return `All tasks will be successfully completed for ${workingDays-workingResult} days before the deadline!`;
    return `The development team have to spend extra ${(workingResult-workingDays)*8} hours after deadline to complete all backlog tasks`
}



console.log(getDeadline(workers,tasks,deadline));
