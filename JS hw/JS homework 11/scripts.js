const buttons = document.querySelectorAll('.btn');
const buttonsText = [...document.querySelectorAll('.btn')].map(item => item.innerText.toUpperCase());
document.addEventListener('keydown', function (event) {
if (!event.key) {
    buttons.forEach(item => {
        item.style.backgroundColor = 'black';
    });
}
    if (!buttonsText.includes(event.key.toUpperCase())) return;
    buttons.forEach(item => {
    if (event.key.toUpperCase() === item.innerText.toUpperCase()){
        item.style.backgroundColor = 'blue';
    }
})
});
document.addEventListener('keyup', function () {
        buttons.forEach(item => {
            item.style.backgroundColor = 'black';
        });
});