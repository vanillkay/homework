
function createNewUser() {
    let name = prompt("Enter your name, please");
    while (!(isNaN(+name))){
        name = prompt("Enter your name, please");
    }
    let surName = prompt("Enter your surname, please");
    while (!(isNaN(+surName))){
        surName = prompt("Enter your surname, please");
    }
    let obj =  {
        name: name,
        surName: surName,
        getLogin(){
            return (this.name[0]+this.surName).toLowerCase();
        },
        setName(newName){
            Object.defineProperties(this,{
            name:{
                writable: true,
            }
            });
            this.name = newName;
            Object.defineProperties(this,{
                name:{
                    writable: false,
                }
            });
        },
        setSurName(newSurName){
            Object.defineProperties(this,{
                surName:{
                    writable: true,
                }
            });
            this.surName = newSurName;
            Object.defineProperties(this,{
                surName:{
                    writable: false,
                }
            });
        },
    };
    Object.defineProperties(obj,{
       name:{
           writable: false,
       } ,
        surName:{
           writable: false,
        },
    });
    return obj;
}



let user = createNewUser();
console.log(user);
console.log(user.getLogin());