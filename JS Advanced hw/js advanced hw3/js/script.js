document.addEventListener('DOMContentLoaded', function (event){
    class Employee {
        constructor({name, age, salary} = {}) {
            this._name = name;
            this._age = age;
            this._salary = salary;
        }
        get name(){
            return this._name;
        }

        set name(newName){
            this._name = newName;
        }

        get age(){
            return this._age;
        }

        set age(newAge){
            this._age = newAge;
        }

        get salary(){
            return this._salary;
        }

        set salary(newSalary){
            this._salary = newSalary;
        }
    }
    const employee = new Employee();
    console.log(employee);

    class Programmer extends Employee{
        constructor({lang, ...params} = {}) {
            super(params);
            this._lang = lang;
        }

        get salary(){
            return this._salary * 3 ;
        }
    }

    const programmer = new Programmer({name:'Peter', age: 19, salary: 300, lang: ['C', 'C++']});
    const programmer2 = new Programmer({name:'Tony', age: 21, salary: 500, lang: ['JavaScript', 'Java', 'Python']});
    const programmer3 = new Programmer({name:'Johny', age: 24, salary: 400, lang: ['PHP', 'Ruby', 'Julia', 'GO']})
    console.log(programmer);
    console.log(programmer2);
    console.log(programmer3);
});