document.addEventListener('DOMContentLoaded', function (){
        const ipButton = document.createElement('button');
        ipButton.innerText = 'Вычислить по IP';
        document.body.append(ipButton);
        ipButton.addEventListener('click', findByIP);
    function findByIP(){
        fetch('https://api.ipify.org/?format=json')
            .then(info => info.json())
            .then(({ip}) => {
                fetch(`http://ip-api.com/json/${ip}?lang=ru`)
                    .then(data => data.json())
                    .then(info => showInfo(info));
            });
    }
    function showInfo(data){
        if(ipButton.nextElementSibling) ipButton.nextElementSibling.remove();
        const divElement = document.createElement('div');
        const neededParams = ['timezone', 'country', 'region', 'city', 'regionName'];
        for (let value in data){
            if (!neededParams.includes(value)) continue;
            const pElem = document.createElement('p');
            pElem.innerText = `${value} – ${data[value]}`;
            divElement.append(pElem);
        }
        ipButton.after(divElement);
    }
})