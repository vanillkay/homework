document.addEventListener('DOMContentLoaded', function (){
        const ipButton = document.createElement('button');
        ipButton.innerText = 'Вычислить по IP';
        document.body.append(ipButton);
        ipButton.addEventListener('click', findByIP);
    async function findByIP(){
        const response = await fetch('https://api.ipify.org/?format=json');
        const {ip} = await response.json();
        const idResponse = await fetch(`http://ip-api.com/json/${ip}?lang=ru`);
        const idInfo = await idResponse.json();
        showInfo(idInfo);
    }

    function showInfo(data){
        if(ipButton.nextElementSibling) ipButton.nextElementSibling.remove();
        const divElement = document.createElement('div');
        const neededParams = ['timezone', 'country', 'region', 'city', 'regionName'];
        for (let value in data){
            if (!neededParams.includes(value)) continue;
            const pElem = document.createElement('p');
            pElem.innerText = `${value} – ${data[value]}`;
            divElement.append(pElem);
        }
        ipButton.after(divElement);
    }
})