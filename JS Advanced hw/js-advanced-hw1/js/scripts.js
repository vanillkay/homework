document.addEventListener('DOMContentLoaded', function () {
    // //Ex 1
    // const arr = ["Kyiv", "Berlin", "Dubai", "Moscow", "Paris"];
    // console.log(...arr);
    //
    // //Ex 2
    // const Employee = {
    //     name: 'Ivan',
    //     salary: "100$"
    // }
    // const {name, salary} = Employee;
    // console.log(name, salary);
    //
    // //Ex 3
    // const array = ['value', 'showValue'];
    // const [value, showValue] = array;
    // alert(value);
    // alert(showValue);




    const characters = [
        {
            name: "Елена",
            lastName: "Гилберт",
            age: 17,
            gender: "woman",
            status: "human"
        },
        {
            name: "Кэролайн",
            lastName: "Форбс",
            age: 17,
            gender: "woman",
            status: "human"
        },
        {
            name: "Аларик",
            lastName: "Зальцман",
            age: 31,
            gender: "man",
            status: "human"
        },
        {
            name: "Дэймон",
            lastName: "Сальваторе",
            age: 156,
            gender: "man",
            status: "vampire"
        },
        {
            name: "Ребекка",
            lastName: "Майклсон",
            age: 1089,
            gender: "woman",
            status: "vempire"
        },
        {
            name: "Клаус",
            lastName: "Майклсон",
            age: 1093,
            gender: "man",
            status: "vampire"
        }
    ];


    // const newArr = characters.reduce((prevArr, {name, lastName, age}) => {
    //     return [...prevArr, {name, lastName, age}];
    // }, []);




    // const user1 = {
    //     name: "John",
    //     years: 30
    // };
    //
    // const {name, years, isAdmin = false} = user1;
    //
    // console.log(name);
    // console.log(years);
    // console.log(isAdmin);


    // const satoshi2020 = {
    //     name: 'Nick',
    //     surname: 'Sabo',
    //     age: 51,
    //     country: 'Japan',
    //     birth: '1979-08-21',
    //     location: {
    //         lat: 38.869422,
    //         lng: 139.876632
    //     }
    // }
    //
    // const satoshi2019 = {
    //     name: 'Dorian',
    //     surname: 'Nakamoto',
    //     age: 44,
    //     hidden: true,
    //     country: 'USA',
    //     wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
    //     browser: 'Chrome'
    // }
    //
    // const satoshi2018 = {
    //     name: 'Satoshi',
    //     surname: 'Nakamoto',
    //     technology: 'Bitcoin',
    //     country: 'Japan',
    //     browser: 'Tor',
    //     birth: '1975-04-05'
    // }
    //
    // const fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020};
    //
    // console.log(fullProfile);




    // const employee = {
    //     name: 'Vitalii',
    //     surname: 'Klichko'
    // }
    //
    //
    // const newObj = {...employee, age: 22, salary: 300}

//
//     const array = ['value', () => 'showValue'];
//
// // Допишите ваш код здесь
//
//     const [value, showValue] = array;
//
//     alert(value); // должно быть выведено 'value'
//     alert(showValue());  // должно быть выведено 'showValue'


    let a =2;
    let b =3;

    [a, b] = [b,a];

    console.log('a->', a,'b->', b)




})