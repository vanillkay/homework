document.addEventListener('DOMContentLoaded', function (){

    const books = [
        {
            author: "Скотт Бэккер",
            name: "Тьма, что приходит прежде",
            price: 70
        },
        {
            author: "Скотт Бэккер",
            name: "Воин-пророк",
        },
        {
            name: "Тысячекратная мысль",
            price: 70
        },
        {
            author: "Скотт Бэккер",
            name: "Нечестивый Консульт",
            price: 70
        },
        {
            author: "Дарья Донцова",
            name: "Детектив на диете",
            price: 40
        },
        {
            author: "Дарья Донцова",
            name: "Дед Снегур и Морозочка",
        }
    ];

    const properties = ['author', 'name', 'price'];

    function sortAndInsertArr(arr, neededProps){
        const list = document.createElement('ul');
        arr.forEach((item, index) => {
            try{
                neededProps.forEach(prop => {
                    if ( !(prop in item) ) throw new Error(`There is no property ${prop} in ${index+1} object`);
                })
                const listElem = document.createElement('li');
                listElem.innerText = `Book: "${item.name}"\n author: ${item.author}\n price: ${item.price}`;
                list.insertAdjacentElement('beforeend', listElem);
            }catch (error){
               console.log(error.message);
            }
        })
        return list;
    }

    function showListFromArr(arr, sortProp){
        const readyList = sortAndInsertArr(arr, sortProp);
        document.body.append(readyList);
    }

    showListFromArr(books, properties);
});