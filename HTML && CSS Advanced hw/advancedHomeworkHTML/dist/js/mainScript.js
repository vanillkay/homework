$(document).ready(function () {
    $('.menu-btn').on('click', function (event) {
    event.preventDefault();
    $(this).toggleClass('menu-btn_active');
    $('.menu-nav__link').each(function () {
        $(this).slideToggle(400, function () {
        })
    })
    });
    function windowSize(){
        if ($(window).width() <= 768){
            $('#anotherPost').hide();
            $('.menu-nav__link').hide();
            $('.menu-btn').show();
            $('.posts__text').hide();
            $('.insta-posts__item').each(function (index) {
                if (!index) return;
                $(this).hide()
            });
            $('.title-invitation').hide();
        } else {
            $('.title-invitation').show();
            $('.menu-nav__link').show();
            $('.menu-btn').hide();
            $('#anotherPost ').show();
            $('.posts__text').show();
            $('.insta-posts__item').each( function (index) {
                if (index === 3 && $(window).width() < 1200) return;
                $(this).show();
            });
        }
    }
    $(window).on('load resize', windowSize);
});