const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const del = require('del');
const autoprefixer = require('gulp-autoprefixer');
const clean = require('gulp-clean');
const minify = require('gulp-minify');
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();

const moveCSS = () => {
    return gulp.src(["src/scss/mainStyles.scss", "src/css/normalize.css"])
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(gulp.dest("dist/css"))
};

gulp.task("moveCSS", moveCSS);
gulp.task('watchCSS', function () {
    return gulp.watch("src/scss/**/*.scss", moveCSS);
});
gulp.task('dev', ['browserSync'], function () {
    gulp.watch(['src/scss/**/*.scss', 'src/js/**.*.js'],moveCSS);
});

gulp.task('build', function () {
    del.sync('dist');
    gulp.src(["src/scss/mainStyles.scss", "src/css/normalize.css"])
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], { cascade: true }))
        .pipe(clean())
        .pipe(gulp.dest("dist/css"));
        gulp.src('src/js/*js')
        .pipe(minify())
        .pipe(gulp.dest('dist/js'));
        gulp.src('src/assets/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
});
